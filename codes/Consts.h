#pragma once

#include <bitset>
#include <cstddef>

#define GF_CHAR 2

const size_t size = 9;
using elem_t = uint16_t;
const elem_t max_value = GF_CHAR - 1;

const size_t max_significant_positions = (size - 1) * size / 2;
using bit_dump = std::bitset<max_significant_positions>;
