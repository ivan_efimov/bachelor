#include <iostream>
#include <fstream>
#include <sstream>
#include <csignal>
#include <vector>
#include <future>

#include <eigen3/Eigen/Dense>

#include "Enumerator.h"
#include "CheckStrictReal.h"

void sigh(int sig)
{

}

size_t provide_matrs_n_invs(std::ifstream& input, async::safe_queue<strict_real::matr_n_inv>& out_queue)
{
	size_t counter = 0;
	bit_dump m_, inv_;
	while (input) {
		if (counter == 20) {
			break;
		}
		input >> m_ >> inv_;
		if (input.eof()) {
			break;
		}
		out_queue.push(std::make_pair(m_, inv_));
		counter++;
	}
	out_queue.close();
	return counter;
}

size_t check(async::safe_queue<strict_real::matr_n_inv>& in_queue, async::safe_queue<strict_real::matr_n_inv>& out_queue)
{
	Eigen::Matrix<elem_t, size, size> m;
	m.setIdentity();
	enm::enumerator m_enm;
	for (int i_ = size - 1; i_ > 0; i_--) {
		for (int j_ = i_ - 1; j_ >= 0; j_--) {
			m_enm.positions.push_back({
				                          .i = i_,
				                          .j = j_,
				                          .value = &m(i_, j_),
			                          });
		}
	}

	Eigen::Matrix<elem_t, size, size> inv;
	inv.setIdentity();
	enm::enumerator inv_enm;
	for (int i_ = size - 1; i_ > 0; i_--) {
		for (int j_ = i_ - 1; j_ >= 0; j_--) {
			inv_enm.positions.push_back({
				                          .i = i_,
				                          .j = j_,
				                          .value = &inv(i_, j_),
			                          });
		}
	}

	size_t checked_counter = 0;


	Eigen::Matrix<elem_t, size, size> im;
	Eigen::Matrix<elem_t, size, size> im_sq;

	std::ofstream failed("failed.txt");

	while (true) {
		auto [data, open] = in_queue.pop();
		if (!open) {
			break;
		}
		checked_counter++;
		m_enm.from_bits(data.first);
		inv_enm.from_bits(data.second);

		std::cerr << data.first << " " << data.second << "\n";
		std::cerr << m << "\n&\n" << inv << "\n\n" << std::endl;
		im = (inv * m).unaryExpr([](elem_t e) -> elem_t { return e % 2; });
		im_sq = (im * im).unaryExpr([](elem_t e) -> elem_t { return e % 2; });
		if (!im_sq.isIdentity()) {
			out_queue.push(data);
			failed << m << "\n&\n" << inv << "\n\n";
		}
	}
	out_queue.close();
	failed.close();

	return checked_counter;
}

int main(int argc, char** argv)
{
	if (argc != 2) {
		std::cout << "invalid arguments!\n";
		exit(1);
	}

	signal(SIGINT, sigh);

	std::ifstream in(argv[1]);
	if (!in) {
		std::cout << "Can't open a file!\n";
		exit(1);
	}

	async::safe_queue<strict_real::matr_n_inv> feed_queue;
	async::safe_queue<strict_real::matr_n_inv> failed_queue;

	auto f1 = std::async(std::launch::async, provide_matrs_n_invs, std::ref(in), std::ref(feed_queue));
	auto f2 = std::async(std::launch::async, check, std::ref(feed_queue), std::ref(failed_queue));

	size_t failed_counter = 0;
	while (true) {
		auto [data, open] = failed_queue.pop();
		if (!open) {
			break;
		}
		std::cout << data.first << " " << data.second << std::endl;
		failed_counter++;
	}
	std::cout << "total failed: " << failed_counter << std::endl;
	auto total_read = f1.get();
	auto total_checked = f2.get();

	std::cout << "total read: " << total_read << ", checked: " << total_checked << std::endl;
	in.close();
}
