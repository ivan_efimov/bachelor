#pragma once

#include <deque>
#include <unordered_set>

#include "Consts.h"

namespace gm {

size_t generate(std::deque<bit_dump>& out, const std::unordered_set<bit_dump>& already_done);

}