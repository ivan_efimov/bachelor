#pragma once

#include "SafeQueue.h"

#include "Consts.h"

namespace strict_real {

using matr_n_inv = std::pair<bit_dump, bit_dump>;

void check(async::safe_queue<strict_real::matr_n_inv>& out, std::deque<bit_dump>& matrices, std::deque<bit_dump>& involutions, size_t th_count);

}
