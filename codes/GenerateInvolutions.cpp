#include "GenerateInvolutions.h"

#include <mutex>
#include <future>

#include "Enumerator.h"
#include "UTMatrix.h"

namespace gi {


size_t generate_thread(std::deque<bit_dump>& out, size_t step, size_t offset, std::mutex& m) {
	ut::ut i;

	ut::set(i, ut::set_identity_ut);

	size_t count = 0;

	enm::enumerator enumerator;
	for (int i_ = size - 1; i_ > 0; i_--) {
		for (int j_ = i_ - 1; j_ >= 0; j_--) {
			enumerator.positions.push_back({
				                               .i = i_,
				                               .j = j_,
				                               .value = &i[i_][j_],
			                               });
		}
	}

	if (!enumerator.increase(offset + 1)) { // Единичная матрица -- не инволюция!
		return 0;
	}

	do {
		if (ut::is_involution(i)) {
			m.lock();
			out.push_back(enumerator.to_bits());
			count++;
			m.unlock();
		}
	} while (enumerator.increase(step));

	return count;
}

size_t generate(std::deque<bit_dump>& out, size_t th_count)
{
	std::mutex out_lock;

	std::vector<std::future<size_t>> thread_results;
	thread_results.reserve(th_count);

	size_t count = 0;

	for (size_t i = 0; i < th_count; i++) {
		thread_results.push_back(std::async(generate_thread, std::ref(out), th_count, i, std::ref(out_lock)));
	}
	for (auto& future : thread_results) {
		count += future.get();
	}

	return count;
}

}