#include "UTMatrix.h"

namespace ut {

elem_t set_identity_ut(size_t i, size_t j)
{
	if (i == j) {
		return 1;
	}
	return 0;
}
void set(ut& out, const std::function<elem_t(size_t, size_t)>& f)
{
	for (size_t i = 0; i < size; i++) {
		for (size_t j = 0; j < size; j++) {
			out[i][j] = f(i, j);
		}
	}
}
void mult_l_constrained(const ut& a, const std::bitset<size>& rows_changed, const ut& b, ut& out)
{
	for (int i = 0; i < size; i++) {
		if (rows_changed[i]) {
			for (size_t j = 0; j < i; j++) {
				elem_t acc = 0;
				for (size_t k = 0 + j; k < i + 1; k++) {
					acc += a[i][k] * b[k][j];
				}
				out[i][j] = acc % (max_value + 1);
			}
		}
	}
}
bool is_involution(const ut& a)
{
	for (size_t i = 0; i < size; i++) {
		for (size_t j = 0; j < i; j++) {
			elem_t acc = 0;
			for (size_t k = 0 + j; k < i + 1; k++) {
				acc += a[i][k] * a[k][j];
			}
			if (acc % 2 != 0) {
				return false;
			}
		}
	}
	return true;
}

}